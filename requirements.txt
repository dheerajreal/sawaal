Django==2.2.6
dj-database-url==0.4.2
django-crispy-forms==1.8.1
whitenoise==5.0.1
gunicorn==20.0.2
python-decouple==3.1